#from sqlite3 import Error
from pymongo import MongoClient

client = MongoClient('localhost')

db = client['Panamenismos']

col = db['palabras']

palabra1 = {"_id":1, "palabra":"Que sopa", "significado":"Como estas"}
palabra2 = {"_id":2, "palabra":"Pollo", "significado":"Novio"}
palabra3 = {"_id":3, "palabra":"Pelo el bollo", "significado":"Se murio"}
palabra4 = {"_id":4, "palabra":"Pinta", "significado":"Cerveza"}

col.insert_many([palabra1, palabra2, palabra3, palabra4])

    
import os
def menu():
    #os.system("cls")
    print ()
    print ()
    print ("Jerga Panamenas")
    print ("palabras que se usan en Panamá mas que en otros países en la vida cotidiana.")
    print ("Selecciona una opcion del siguiente menu:")
    print ("1) AGREGA UNA NUEVA PALABRA")
    print ("2) EDITA UNA PALABRA EXISTENTE")
    print ("3) ELIMINA UNA PALABRA EXISTENTE")
    print ("4) VER LISTADO DE PALABRAS")
    print ("5) BUSCAR SIGNIFICADO DE PALABRA")
    print ("6) SALIR")
    print ()        
        

while True:
    menu()

    # Leemos lo que ingresa el usuario
    opcion = input("Selecciona :")
    print ()
    if opcion == "1":
        print ("Seleccionaste la opcion que agrega una palabra nueva")
        id = int(input("Id: "))
        palabra = input("Palabra: ")       
        significado = input("Signidicado: ")
        col.insert_one({
            '_id': id,
            'palabra': palabra,
            'significado': significado})
                          
    elif opcion == "2":
        print ("Seleccionaste la opcion que modifica una Palabra")
        id = int (input ("Ingresa un ID: "))
        palabra = input ("Ingresa la palabra actualizada: ")
        col.update_one({
            "_id": id
        }, {
            "$set": {
                "palabra": palabra
            }
        })
                    
    elif opcion == "3":
        print ("Seleccionaste la opcion que borra una palabra")
        id = int (input ("Ingresa el ID de la palabra que deseas eliminar: "))
        col.delete_one({
            "_id": id
        })
                
                  
                       
    elif opcion == "4":
        print ("Seleccionaste la opcion que nos muestra todos las palabras en la tabla")
        print ("")
        for panamenismos in col.find({}):
            print(panamenismos)
        
                                
    elif opcion == "5":
        print ("Seleccionaste la opcion que nos busca el significado de la palabra")
        palabra = input ("Ingresa la palabra que quieres saber su significado: ")
        doc = col.find_one({
            "palabra": palabra
        })
        print (doc)
        
            
    elif opcion == "6":
        print ("Si quieres aprender mas Panamanismos vuelve pronto")
        exit()
    else:
        print("opcion invalida")
    
